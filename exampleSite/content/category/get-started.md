---
# In posts use   categories: ["Get Started"]

title: Get Started
subtitle: Get going in a jiffy
feature_image: ""

# A category page must be member of its own category.

categories:
- Get Started
taxonomy: categories

# The description is only used for SEO.

description: |
  The Written Theme is a simple to implement
  theme for Hugo with lots of bells and wistles.
  This category covers all the basics for using
  the theme and gets you started in a jiffy.

# There are two page layouts:
# 
# - index-page    if space permits, shows the feature_image on the left, and
#                 aligns page content to the left.
# - content-page  always shows the feature_image at the top, and centres the
#                 page content.
# 
# You should use the two layouts consistently. All list-style pages (such as
# category and author pages) should use index-page layout. Content pages (such
# as posts) should use content-page layout.

pagelayout: index-page
---

Under the hood, the Written Theme is a very advanced Hugo theme, but it is very
simple to use. The following posts cover all the basics for using this theme and
get your website ready in a jiffy.
