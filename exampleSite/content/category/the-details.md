---
title: The Details
subtitle: For those Hugo-apt.
feature_image: img/category-the-details.jpg

description: >
  Pages filed under The Details describe technical details about the Written
  that usually require a bit of further understanding of Hugo to read.

categories:
- The Details
taxonomy: categories

pagelayout: index-page
---

We set ourselves quite high targets for The Written Theme. Actually, with some
of them we have been very close to giving up (like with making lookup partials
work). Through perseverence and endless hacking hours, we have overcome.

It is with great pride that we present you the following, somewhat technical,
posts describing the innovation behind the scenes. The paper on which all your
blogs are written.
