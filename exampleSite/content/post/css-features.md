---
authors:
- Paul Koppen
categories:
- The Details
date: 2016-02-23T23:24:45Z
description: |
  Written was written to be versatiley static, pushing the boundaries of what
  is possible with pure HTML5 + CSS3.
feature_image: ""
pagelayout: content-page
title: CSS Features
---

Written was designed to be pure HTML5 + CSS3. No scripts. That meant pushing
hard, like seriously hard, on the boundaries of what is possible with CSS, but
we managed to do it.


## No CSS!?

Actually, we pushed *beyond* the limits of CSS. All pages have been tested with
CSS disabled. How is the content presented? Does the ordering of the content
blocks make sense? Does the source read naturally, from top to bottom?

A funny result of these checks is for example the `hr` tag, separating the site
footer from the main content: a boost for non-CSS legibility!

Another example is that in the code the post meta block comes before the
related posts block, while in the browser the related posts are presented
first.

Why this effort? Because without CSS is how most search engines view your
website. If you want to look good in the search results, you better make sure
your content is presented well. Although... Written already does that for you.
One thing less to worry about.


## Site navigation

Did you notice the icon in the top-left hand corner? If you click it, the
[sitenav menu][1] will open, sliding all page content to the side. With the menu
open, wherever you click that is not one of the links, the menu will close.
This is all CSS!

Even better: when you leave the page by clicking a link in the sitenav, and then
come back (press the browser back button, or the backspace key) the sitenav will
be closed again. It's a tiny detail, but massively important for UI design. It
would have been a horrible user experience if the menu were still open, really.


## Tabs

[A configuration option][2] in the site config.yaml allows you to use tabs to
switch between the panel of recent posts and popular categories on the front
page. These tabs work purely in CSS, and work totally with the keyboard. You
can even disable tabs!

The system is easily extensible, so if other pages need it in the future they
can get it straight away. Just put the `.tabbed` class on the container
element.


## Email obfuscation

The sad truth is, there is no way to show your email address on a public website
*and* keep it private at the same time. So publishing it for your visitors means
that web crawlers will be able to get it too.

That said, Written does its best to obfuscate your email address. That means
that while people visiting your site will see it as it is, to crawlers it will
look garbled.

In technical terms, this is done with some CSS trickery and URL-escapes. This
will certainly help. Ultimately though, you will need good spam filters.


## Responsive layout

Noticed the header image (feature image) jumping from side to top and back when
you resize the browser window? It is a beautiful visual style to make pages
read comfortable at different resolutions. It is a popular technique, but still
a valuable one.

And it is not only the header image that adapts to screen size. In fact the
complete site layout adapts itself. It makes sure that even in the browser on
your mobile phone the site looks good. This is a very important feature, not
only to engange with your readers (which should be the first and foremost
reason), but also Google penalises pages that don't have it. So there you go,
another feat Written solves for you.



[1]: {{< relref "post/site-navigation.md" >}}
[2]: {{< relref "post/site-configuration.md#tabs" >}}
