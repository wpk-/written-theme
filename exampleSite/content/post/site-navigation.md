---
authors:
- Paul Koppen
categories:
- Get Started
date: 2016-05-16T14:36:46Z
description: |
  Quickly set up a site navigation with static pages. There is also a footer
  navigation, which is usually similar but not equal to the site navigation.
feature_image: ""
pagelayout: content-page
title: Site Navigation
---

Most websites have a dedicated site navigation that usually appears in all page
headers and footers. It points to some pages such as "About", "Contact Us",
"Terms of Use" and the like.

Although menus are covered in great detail on the [Hugo website][2], we will
present a quick structure for setting this up with Written. Note that Written
does not support nested menus at present.


## Add a page to the sitenav

Let's say we're creating an about page and want to link to it from the site
navigation. First use Hugo to create the page:

{{< highlight shell >}}
hugo new about.md
{{< / highlight >}}

That will create a file that looks roughly like the following:

{{< highlight yaml >}}
---
date: 2016-05-16T16:09:50Z
description: |
  Description of the page for SEO.
linktitle: Default
pagelayout: content-page
standalone: true
subtitle: ""
title: about
---
{{< / highlight >}}


## Menu

Written has two menus: **main** and **footer**. The main navigation is hidden
on the left-hand side of the screen and is revealed by clicking the icon in the
top left corner. The footer navigation is shown at the bottom of each page.
Although the two menus are usually quite similar, they don't have to be.

We can add our about page to both menus by adding the following lines:

{{< highlight yaml >}}
menu:
  main:
    pre: fa-check-square-o
    weight: -80
  footer:
    weight: -80
{{< / highlight >}}

The `weight` is used to configure the order in which items appear in each menu.

The `pre` field adds a [FontAwesome][2] icon to the left of the link text. See
the FontAwesome website for icon names.


## Link Title

Because menus are usually small, it is often desirable to have a very short
title (or just one word) in the navigation. This may be different from the page
title, which can be long.

The following makes sure menu links to our about page stay short:

{{< highlight yaml >}}
title: About this page
linktitle: About
{{< / highlight >}}



[1]: //gohugo.io/extras/menus/
[2]: //fortawesome.github.io/Font-Awesome/
