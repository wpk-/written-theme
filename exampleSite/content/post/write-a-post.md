---
authors:
- Paul Koppen
- Alice
- Tweedle Dee
- Humpty Dumpty
categories:
- Get Started
date: 2016-04-04T12:32:32Z
description: >
  Three simple steps to post a new entry to your Written-themed Hugo blog.
feature_image: feature-book.jpg
pagelayout: content-page
title: Write a Post
---

Writing a new blog post with Hugo is very simple:

{{< highlight shell >}}
hugo new post/*post-title*.md
{{< / highlight >}}

Now, the Written theme provides a few nice bells and whistles. Let's look at an
example post and analyse it step by step. Here is the contents for the post we
just created:

{{< highlight yaml >}}
---
authors:
- Author Name
categories:
- ""
date: 2016-04-04T12:37:12Z
description: |
  A brief description for this
  post, used for SEO.
feature_image: ""
pagelayout: content-page
title: post title
---

Post content goes here (Markdown).
{{< / highlight >}}


## Authors

To start with, the Written theme supports co-authorship, which is why `authors`
is plural and it has a list of names.

Furthermore, the author names will be linked to the pages you create in the
`author/` folder to get things like avatars (profile photos). Those pages are
called [author pages][1], and are completely optional. Follow the link to read
more.

An example of a post with multiple authors is this very page. See the bottom of
this page for how they are presented. Only the first author has a dedicated
author page, but all authors have an index page.

On single-author pages, such as [this one][2], the author and category
attribution is aligned to the left.

Authors on this page:

{{< highlight yaml >}}
authors:
- Paul Koppen
- Alice
- Tweedle Dee
- Humpty Dumpty
{{< / highlight >}}


## Categories

There is not much special about this, as most of it is standard Hugo
functionality. However, analogous to author pages, you can also create category
pages to provide content and image data on a per-category basis. The concept
overarching author and category pages we call [term pages][2]. That page
provides a tutorial walking throught the steps of [creating a category page][2].

Categories on this page:

{{< highlight yaml >}}
categories:
- The Basics
- Get Started
{{< / highlight >}}


## Description

The page description adds a `<meta name="description" content="...">` tag to
the HTML `<head>` section. Although debated, this is believed to improve
[SEO][3].

Description for this page:

{{< highlight yaml >}}
description: >
  Three simple steps to
  post a new entry to
  your Written-themed
  Hugo blog.
{{< / highlight >}}


## Feature image

The header on every page in the Written theme provides space for a large image.
When no feature is defined, the default feature image is taken from the
`feature_image` parameter in `config.yaml`.

Upload feature images to the `static/img/` folder.

Depending on the choice of page layout (discussed below) the image should be
large enough to serve both full width and full height. Although it will always
be scaled to fit, too much stretching may make the image pixely.

Feature image on this page (`static/img/feature-book.jpg`):

{{< highlight yaml >}}
feature_image: feature-book.jpg
{{< / highlight >}}

Notice that `static/img/` is omitted in the declaration. It is automatically
prepended.


## Page layout

The Written theme has two layout modes: `content-page` and `index-page`. As the
names suggest, one is used for pages whose main massage is content, whereas the
other is used on pages presenting an index list to other pages.

### Content page layout

The header (image) on content pages *always* sits at the top, stretching the
full page width. It is smaller than the header image on index pages.

On wider screens page content is centred in a fixed-width column. On smaller
screens the column adapts to fit the device.

The current page is an example of a `content-page` layout.

{{< highlight yaml >}}
pagelayout: content-page
{{< / highlight >}}

### Index page layout

The header (image) for index pages is slightly larger than that on content
pages. More importantly, on wide screens it moves to the left of the content,
stretching the full viewport height and adopting static positioning. The page
title and subtitle are then printed at the bottom.

On smaller screens the header moves back to the top, similar to content pages,
and the links adapt their size to fit the device width.

Examples of `index-page` layout are the [front page][4] and the page listing
[all posts by Paul Koppen][5].


[1]: {{< relref "post/author-pages.md" >}}
[2]: {{< relref "post/term-pages.md" >}}
[3]: https://en.wikipedia.org/wiki/Search_engine_optimization#Increasing_prominence
[4]: {{< relref "index.md" >}}
[5]: /authors/paul-koppen