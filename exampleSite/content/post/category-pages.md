---
authors:
- Paul Koppen
categories:
- Get Started
date: 2016-05-16T10:20:54Z
description: |
  Learn how to style category pages that list all posts in that category. Add
  text, images, a header image, etc.
feature_image: ""
pagelayout: content-page
title: Category Pages
---

When you write a blog post and add it to a category, Hugo will automatically
create another page to list all posts in that category. With the Written theme,
we can style that category page very easily.


## Create a category page

Use Hugo to create a post in the category folder:

{{< highlight shell >}}
hugo new category/*category-name*.md
{{< / highlight >}}

This creates a page just like any other page, except that it does not show up on
the general list of blog posts. Also, next to any customisations we make (as
described below), it will list all pages in the category.

The file we just created looks something like:

{{< highlight yaml >}}
---
categories:
- Category Name
date: 2016-05-16T10:27:05Z
description: |
  A brief description for the category, used for SEO.
feature_image: ""
pagelayout: index-page
subtitle: ""
taxonomy: categories
title: category name
---
Some text about the category.
{{< / highlight >}}

As you can see, the properties are very much the same as for [posts][1] and
[author pages][2], though a category page does not have any authors.

We go through the properties one by one:


## Categories

The declaration `categories` is plural. However, it is essential to make a
category page only member of its own category. For the technical background on
this see [Term Pages][3].

{{< highlight yaml >}}
categories:
- The Basics
taxonomy: categories
{{< / highlight >}}

This forms the semantic definition of the page. The actual displayed text is set
using the `title` and `subtitle` properties:

{{< highlight yaml >}}
title: The Basics
subtitle: Get going in a jiffy
{{< / highlight >}}

As you would expect, the subtitle is printed in the header right below the title
in a slightly smaller font.


## Content

The content (below the three hyphens) is shown just as on any other page.
Because the category page is an index page, below the content a list will be
displayed linking to the most recent posts in that category.


## Description

As with any post, the description is used for search engine optimisation. It
adds a `<meta name="description" content="...">` tag to the HTML `<head>`
section. See also [Write a Post][1].

{{< highlight yaml >}}
description: >
  This category covers all the basics
  for using the theme.
{{< / highlight >}}


## Feature image

On index pages (which the category page is, because is shows a list of links to
content pages), the feature image covers a full-height column on the left hand
side of the screen. On smaller devices, it moves to the top of the page. Because
it is both used horizontally and vertically, it is advised to use relatively
large feature images on index pages.

Copy the image to `static/img/category-name-feat.jpg` and use it with the
following code:

{{< highlight yaml >}}
feature_image: category-name-feat.jpg
{{< / highlight >}}

Notice that the path `static/img/` is not part of the declaration. It is
automatically prepended.


## Further reading

The concept underpinning category and author pages is the [Term Page][3]. Follow
the link for the technical details of how The Written Theme adds content to
index pages.



[1]: {{< relref "post/write-a-post.md" >}}
[2]: {{< relref "post/author-pages.md" >}}
[3]: {{< relref "post/term-pages.md" >}}
