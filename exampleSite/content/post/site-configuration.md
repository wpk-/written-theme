---
authors:
- Paul Koppen
categories:
- The Details
date: 2016-05-16T15:33:32Z
description: |
  The Written theme has a few specific site configuration options that weight
  describe here.
feature_image: ""
pagelayout: content-page
title: Site Configuration
---

The following configuration options are specific to the Written theme, and use
`config.yaml`. For general [site configuration in Hugo][1] see that page.


## Description

The page description adds a `<meta name="description" content="...">` tag to
the HTML `<head>` section.

In `config.yaml` you can set a default description that will be used when a
page does not define its own.

{{< highlight yaml >}}
params:
  description: This is the Written theme.
{{< / highlight >}}


## Feature image

On every page, the header provides space for a large feature image. On content
pages and on small screens, this feature image covers the full-width window top.
On index pages on large screens, the feature image covers a full-height column
to the left.

In principle, each page defines its own feature image. The feature image defined
in the site configuration is used as a default. Because it can be used on both
content and index pages, make sure to choose your image suitable for both views.

Upload feature images to the `static/img/` folder, e.g.
`static/img/hero_main.jpg`. Then add the following configuration to
`site.yaml`:

{{< highlight yaml >}}
params:
  feature_image: hero_main.jpg
{{< / highlight >}}

Notice that `static/img/` is omitted. It is automatically prepended.


## Contact details

Contact details, when set, are printed on every page in the site footer.

{{< highlight yaml >}}
params:
  email: written@paulkoppen.com
  twitter: Paul_Koppen
{{< / highlight >}}


## Tabbed

The front page shows two blocks with links: one for pages and one for
categories. By default the categories block is displayed below the pages block.
By switching to a tabbed interface, the categories are "behind" the pages, and
the user can switch between the two blocks with the mouse or keyboard. These
are [pure CSS tabs][2]!

To switch to tabs, go to the site config.yaml and add the following parameter:

{{< highlight yaml >}}
params:
  tabbed: true
{{< / highlight >}}

To switch back to the original layout either remove the parameter or set it to
`false`.


## Taxonomies

Written uses two taxonomies: authors and categories. They are defined in
`site.yaml`. This is not so much of a configuration option, but more like
general information about the Written theme.

{{< highlight yaml >}}
taxonomies:
  author: authors
  category: categories
{{< / highlight >}}



[1]: //gohugo.io/overview/configuration/
[2]: {{< relref "post/css-features.md#tabs" >}}
