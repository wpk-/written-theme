---
authors:
- Paul Koppen
categories:
- The Details
date: 2016-05-17T10:55:46Z
description: |
  userhead.html and userfoot.html are two files to add custom HTML code to all
  pages in your website.
feature_image: ""
pagelayout: content-page
title: Customise
---

Written supports two points for site-wide customisation. You can add HTML to the
head in:

    layouts/partials/head_extra.html

and add HTML just before body close in:

    layouts/partials/foot_extra.html


## Example

Let's add a stylesheet that makes all text red.

First create the CSS file in `static/css/red.css` with the following content:

{{< highlight css >}}
body { color: red; }
{{< / highlight >}}

Then insert the link to this file in the site head. Add the following line to
`layouts/partials/head_extra.html`: (notice that the `static/` folder is omitted
from `href`)

{{< highlight html >}}
  <link rel="stylesheet" type="text/css" href="css/red.css">
{{< / highlight >}}

Now run `hugo` to regenerate the website and be amazed by the red stuff!
