---
authors:
- Paul Koppen
categories:
- The Details
date: 2016-04-03T15:48:57Z
description: >
  Written extends the Hugo taxonomy structure by introducing taxonomy term
  pages, which provide content to enrich index pages.
feature_image: ""
pagelayout: content-page
title: Term Pages
---

This blog uses categories to group related posts. So categories is a *taxonomy*.
This very page is filed under The Details category. "The Details" is called a
taxonomy *term*.

When generating the site, Hugo collects all posts that share the same taxonomy
term together and generates an **index page** for it. See for example
this [index page][2] for The Details category.

By default however, Hugo provides no mechanism to add content to index pages
(such as a description or leading text) or add a specific feature image. This is
only available to **content pages** (such as this very page).

In the Written theme we solved that by introducing **term pages**. They bridge
the gap between index and content pages.

As an example for explaining taxonomy pages, we will create a new category page
for the category term "The Details". In the Written theme, `authors` is also a
taxonomy, so [adding an author page][5] is much the same.


## Taxonomy term pages

Create a content page in the folder `category` (note this is singular):

{{< highlight shell >}}
hugo new category/the-details.md
{{< / highlight >}}

Its Markdown body will serve as text content and its YAML front-matter adds
properties such as the feature image. Below is the front-matter copied from
`category/the-details.md` on this site:

{{< highlight yaml >}}
title: The Details
subtitle: For those Hugo-apt.
feature_image: img/category-the-details.jpg

description: >
  Pages filed under The Details describe
  technical details about the Written
  that usually require a bit of further
  understanding of Hugo to read.

categories:
- The Details
taxonomy: categories

pagelayout: index-page
{{< / highlight >}}

First we declare the title and optionally subtitle. They will be printed at the
top or on the feature image. See [here][2]. The description will appear in the
HTML `<head>` section as a meta tag for SEO.

The crucial part is the following block of lines:

{{< highlight yaml >}}
categories:
- The Details
taxonomy: categories
{{< / highlight >}}

which allows the page to be looked up from the *index page*. The lines

1. declare this page to be member of *The Details*, so it appears in
  `.Data.Pages`.
2. flag this page to be a taxonomy term page in the categories taxonomy. The
  actual term is inferred from its membership.


## Content lookup

The index page template runs through `.Data.Pages` and finds the one page with
`taxonomy` set to `categories`. From this page we read the `feature_image` and
any other relevant properties. Content lookup is done with [lookup partials][4].

Note that we now have *two* pages for The Details category: the [content
page][3] as well as the [index page][2]. To align the design principles of the
Written theme with Hugo, the content page should focus on describing the
category, and thus its main body is the `.Content`. It provides a link to
"view all posts" which then points to the index page. The main focus of the
index page is to list all pages, and thus the `.Content` is not shown. Both
pages, however, share the same `title`, `subtitle` and `feature_image`.


## Author pages

In this post we used the `categories` taxonomy as an example. The exact same
concept is also used for authors, where of course one would have to declares
`taxonomy: authors` in the front-matter, as well as `authors: ["Author Name"]`.
See [author pages][5] for more details on that.



[1]: //gohugo.io/themes/creation
[2]: /categories/the-details
[3]: /category/the-details
[4]: {{< relref "post/lookup-partials.md" >}}
[5]: {{< relref "post/author-pages.md" >}}
