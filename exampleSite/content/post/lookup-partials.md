---
authors:
- Paul Koppen
categories:
- The Details
date: 2016-02-24T00:06:15Z
description: |
  Lookup partials are partials that, instead of generating HTML content, contain
  logic to look items, or ranges of items, up and store the results in Scratch.
feature_image: ""
pagelayout: content-page
title: Lookup Partials
---

In [Hugo][1], partials are a type of template that render small pieces of
content based on some parameters. In this post we deal with the issue of
using partials slightly differently; namely to run small pieces of *logic*
(functions), rather than render content (HTML).

It is often desired to separate logic from content. Currently however, Hugo has
no instruments for defining logic specifically, so our only option is to
(somewhat ab-)use partials. Hello Lookup Partials.


## Input

The arguments that are passed in to the lookup partial are, as with any other
partials, determined by the arguments set in the `{partial ...}` declaration,
and so defined what is available in the local scope.


## Output

The output is stored in `.Scratch` and so can be any kind of variable, be it
HTML text, a content record, or even whole lists of posts.

To not clutter the scratch space too much, every partial should only store
output under `.Scratch.*partial-name*`. This also makes the code that performs
the lookup easier to read.


## Examples

### feature_image

{{< highlight go >}}
{{ partial "lookup/feature_image.html" . }}
{{< / highlight >}}

On any page --content or index page-- finds the most suitable `feature_image`
and stores it in `.Scratch.feature_image`.

### find_allposts

{{< highlight go >}}
{{ partial "lookup/find_allposts.html" . }}
{{< / highlight >}}

Reads `.Data.Pages`, which on an taxonomy index page lists all pages under the
specific taxonomy term (e.g. all pages in category "The Details"), and filters
out any page that has the `taxonomy` field defined. It thus removes all term
pages.

Stores the result in `.Scratch.find_allposts`.


## List of lookup partials

Lookup partials live in the aptly named folder `partials/lookup/`. The following
list describes them one by one [incomplete]:

- `feature_image.html` finds the feature image for any Page or Node. Looks for one on the page itself, then on the category content page, and lastly in `site.yaml`.
- `post_related.html` currently returns the latest 5 posts, but in the future when Hugo has the relatedness measure implemented, this should use the built-in Hugo approach to fetch related pages.
- `term_termpage.html` finds the content page associated with a list node. For example /categories/sunday-lunch/ -> /category/sunday-lunch.md, but then returns the actual page object so you can access its properties.
- `termpage_posts.html` given a taxonomy [term page][2] (the content page) finds the most recent posts under that term.



[1]: //gohugo.io/
[2]: {{< relref "post/term-pages.md" >}}
