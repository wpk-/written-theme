---
authors:
- Paul Koppen
- Alice
- Humpty Dumpty
- Tweedle Dee
categories:
- Get Started
date: 2016-04-04T13:46:56Z
description: >
  Everything you need to know about authorship and the authors database in the
  Written theme.
feature_image: ""
pagelayout: content-page
title: Author Pages
---

Author pages provide space for a bio and social (or other) links. They also
include a list of recent posts (see for example, [Paul Koppen][1]). We
demonstrate below how to set it up.

Author pages are optional, however. Without an author page, links to the author
present the full list of their publications without further customisation.


## Create an author page

Use Hugo to create a post in the author folder:

{{< highlight shell >}}
hugo new author/*author-name*.md
{{< / highlight >}}

It is that simple. Let's look at an example author page and analyse it step by
step.

Below is the contents for the author page we just created. At the end of this
tutorial page, the resulting author page should match [this example][1].

{{< highlight yaml >}}
---
authors:
- Author Name
avatar:
  bgcolor: none
  image: ""
  initials: AN
  textcolor: black
date: 2016-05-04T08:49:43Z
description: |
  Use this description for ...
feature_image: ""
links:
  Homepage:
    link: null
    title: null
  Twitter:
    link: null
    title: null
pagelayout: index-page
subtitle: Writer of the past
taxonomy: authors
title: author name
---
{{< / highlight >}}


## Authors

It might look odd at first, to declare a list of authors for a page about one
specific author. This is because `authors` is a taxonomy. Therefore, declaration
of authors is done in plural, even if there is just one author. This is
explained in more detail in [Term Pages][2] and also interesting may be the
authors section in [Write a Post][3].

In any case, for an author page we declare the single author, and add the page
to the authors taxonomy:

{{< highlight yaml >}}
authors:
- Paul Koppen
taxonomy: authors
{{< / highlight >}}

This is purely semantical. The name as it appears on the author page is defined
using the page `title`. Optionally you can use the `subtitle` field to add
extra text like a degree or slogan to the author page header:

{{< highlight yaml >}}
title: Paul Koppen
subtitle: Research Fellow
{{< / highlight >}}


## Avatar

Written provides different avatar options. You can upload an image to serve as
profile photo, or simply print your initials (one or two) on a coloured
background. If neither is defined, by default a grey circle is used with the
first character of your name.

### Profile photo

To set up a profile photo, place the image in `static/img/author-name.jpg`,
then use the following avatar declaration:

{{< highlight yaml >}}
avatar:
  image: author-name.jpg
{{< / highlight >}}

### Initials

For initials printed in white on a green background, use the following:

{{< highlight yaml >}}
avatar:
  bgcolor: green
  initials: PK
  textcolor: white
{{< / highlight >}}

The colours can be any valid [CSS colour declaration][4]. Initials should be
one or two characters.


## Description

As with any post, the description is used for search engine optimisation. It
adds a `<meta name="description" content="...">` tag to the HTML `<head>`
section. See also [Write a Post][5].

{{< highlight yaml >}}
description: >
  Paul Koppen received his PhD in from
  the University of Surrey and ...
{{< / highlight >}}


## Feature image

This will be shown big and wide across the top of the page. Make sure this
image is of sufficient resolution to show nicely, while still keeping the file
size manageable (best below 1MB).

Copy the image to `static/img/author-name-feat.jpg` and use it with the
following code:

{{< highlight yaml >}}
feature_image: author-name-feat.jpg
{{< / highlight >}}

Notice that the path `static/img/` is not part of the declaration. It is
automatically prepended.


## Links

Finally, add any (social) links or other items to a list at the end of the
author page.

{{< highlight yaml >}}
links:
  homepage:
    title: "paulkoppen.com"
    link: "http://paulkoppen.com/"
  twitter:
    title: "@Paul_Koppen"
    link: "//twitter.com/Paul_Koppen"
    rel: nofollow
  fun fact:
    title: "Can whistle in 6 different ways."
{{< / highlight >}}

The `link` and `rel` attributes are optional. See [Link types][6] for a list of
defined `rel` values and their use.



[1]: {{< relref "author/paul-koppen.md" >}}
[2]: {{< relref "post/term-pages.md" >}}
[3]: {{< relref "post/write-a-post.md#authors" >}}
[4]: https://developer.mozilla.org/docs/Web/CSS/color_value
[5]: {{< relref "post/write-a-post.md" >}}
[6]: https://developer.mozilla.org/docs/Web/HTML/Link_types
