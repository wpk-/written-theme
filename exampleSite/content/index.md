---
title: "Written"
subtitle: "A Hugo theme designed for authors."
date: 2015-03-03
description: "Written is a versatile multi-author theme with lots of goodies."
ishome: true
menu:
  main:
    pre: "fa-home"
    weight: -200
pagelayout: index-page
standalone: true
---

{{< logo Written >}} aims at simplicity for authors.

Use this site to learn about the Hugo theme, and as a refererence manual when
writing your blog.
