---
# In posts use   authors: ["Paul Koppen"]

title: Paul Koppen
subtitle: Project Manager
feature_image: ""

# Optional attributes:
links:
  homepage:
    title: "paulkoppen.com"
    link: "http://paulkoppen.com/"
  twitter:
    title: "@Paul_Koppen"
    link: "//twitter.com/Paul_Koppen"
    rel: nofollow

# This is weird, but required to register the author to the authors taxonomy.
authors: ["Paul Koppen"]
taxonomy: authors

# Used throughout website:
# Initials must be no more than two characters.
avatar:
  image: ""
  initials: "PK"
  bgcolor: "green"
  textcolor: "white"

# Author page rendering (description field is for SEO):
description: >
  Paul Koppen received his PhD in from the University of Surrey and his MSc and
  BSc in Artificial Intelligence from the University of Amsterdam. His research
  interests include Machine Learning, Pattern Recognition, Machine Vision (3D
  and 2D) and Deep Learning. He currently combines his research efforts with
  being project manager for the research project FACER2VM.

# Author pages are rendered as index pages, i.e., in larger browser windows the
# site header will be on the left.
pagelayout: index-page
---

> A rose is a rose is a rose.
>
> -- <cite>Gertrude Stein</cite>

> **Juliet:**
> What's in a name? That which we call a rose, by any other name, would smell
> as sweet.
>
> -- <cite>Romeo and Juliet (II, ii, 1-2)</cite>
