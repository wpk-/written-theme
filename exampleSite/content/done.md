---
title: A List Of Completed Things
linktitle: "Done"
description: >
  Bragging rights.
menu:
  main:
    pre: "fa-check-square-o"
    weight: -80
  footer:
    weight: -80
pagelayout: content-page
standalone: true
---

Completed items:

- Fix: Exclude standalone pages from recent posts.
- Favicon.
- Embed Google Fonts.
- Google Analytics.
- Custom lines in `<head>`.
- Custom scripts in site close.
- Add a SASS preprocessing step.
- Separate GitHub repo for theme.
- Automatically deal with new taxonomies / taxonomy terms.
- Front page posts rendered with item-nometa partial.
- Front page categories remdered with item-panel partial and fix CSS.
- Front page text comes from the first Page with `.Params.ishome`.
- Fix the "view all posts" link at the bottom of a related posts block.
- Author homepage and twitter. Generalised to "links" on "content pages".
- Frontpage posts in 2x2 grid.
- Flex on super wide screen.
- Responsive CSS.
- Related posts next to post body.
- .item > et al closes ul before "in Category...".
- Standalone content has no related items block.
- First tab selected by default.
- Abstract list containers in own partial (index, list and single repeat their html).
  This should also fix the related posts headers on all other-than-home pages.
- Pull CSS together.


Wish List Fulfilled:

- Avatar initials using ::after{ content: attr(...); }.
- Swith to tabs easily.


CSS Fixes:

- CSS for .aside (e.g. page Funnies)
- Page width 994px in Funnies makes related content jump down.
- Adjust site header and footer widths for content-page without aside (e.g. about).
- Avatars in post items (.aside).
- Meta in post items (.aside).
- Bottom margin on .aside on front page.
- Subtitle offset on small screen (e.g. author pages).
- Check margins around h1 in site header. > look fine for home page and author page. is there a content page with title in site-header?
- Spacing .small items for related posts (e.g. post/avatars).
- `<pre>` gets scrollbar on narrow screens (see Authors page).
- Category panels should be fully clickable.
- Panels on Firefox.
- Padding/margin of h3 in panels.
- Mobile horizontal scroll.
- Underline in site-header.
- Forms.
