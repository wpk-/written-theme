---
title: A List Of Things To Do
linktitle: "To Do"
description: >
  A note to self for the next Written theme release.
menu:
  main:
    pre: "fa-pencil-square-o"
    weight: -100
  footer:
    weight: -100
pagelayout: content-page
standalone: true
---

For a next release:

- Document `index.md`.
- Document the ishome property.
- Disqus.
- Create relref shortcode variant that links to index pages.
- Expandable menus.
- Add images to `exampleSite`.
- Use alternative code renderer (does not work on Gitlab Pages).

CSS Fixes:

- Fix px units in `_forms.scss` to rem and the like.
- Expand use of variables.


Wish List:

- Social buttons in site header on front page (po.st?).
- Sponsors in site header on front page.
