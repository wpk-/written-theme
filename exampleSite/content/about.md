---
title: "About Written"
linktitle: "About"
description: >
  Written is a comprehensive theme for Hugo. It has multi-author support, pure
  CSS user interaction and many other elegant design features.
menu:
  main:
    pre: "fa-anchor"
    weight: -40
  footer:
    weight: -40
pagelayout: content-page
standalone: true
---

[Written](/) is a very rich [Hugo][5] theme by
[Paul Koppen][1].

Its many many features include a responsive design, dedicated author pages, and
multi-author support. Also, the design is clean, reads well and navigates
easily. All dynamic interaction (such as the site nav menu and tabbed content)
is solved purely with CSS, so there are no scripts.

The theme started out as a Hugo adaptation of the Writer theme for Jekyll, but
has since evolved to something completely independent.

The theme was also my first experience with Hugo, and as such it was a project
to explore the boundaries of what is possible with that platform. Consequently,
the Written theme introduces some novel Hugo concepts and techniques, such as
lookup-partials and optional author pages.

Lastly, the theme was designed to serve as our project home page:
[FACER2VM][2]. I would therefore like to acknowledge the support
from the [University of Surrey][3], and in particular the
[Centre for Vision, Speech and Signal Processing][4].


[1]: {{< relref "author/paul-koppen.md" >}}
[2]: //facer2vm.org
[3]: //surrey.ac.uk
[4]: //www.surrey.ac.uk/cvssp/
[5]: //gohugo.io/
