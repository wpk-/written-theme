written-theme
=============

Static pages written in stone.

Written is a theme for [Hugo][1] featuring

* Multiple authors per blog
* Multiple authors per post
* Advanced author profiles
* Content for category pages
* Pure CSS (no scripts)
* Responsive layout
* Lookup partials
* Google Analytics (optional, adds script)
* much more...


Notes
-----

Before you `npm run` anything, make sure to `git submodule update --init` and
`npm install` (or `npm install --production`).

`npm run optimize` takes a long time. Just wait for it to finish (or don't
optimize; it's optional).


Licence
-------

MIT



[1]: //gohugo.io
