---
# Header
# ======
#
# `title`          Is the title of your post.
# `feature_image`  Big image displayed full-width at the top of the post. Make
#                  sure it is a reasonably sized image (while keeping file size
#                  low).

title: Post Title
feature_image: ""

# Taxonomies
# ==========
#
# `authors`        Link to all authors who contributed to this post.
#                  NB. Use the `authors` value defined in the author page.
# `categories`     You can post to multiple categories.
#                  NB. Use the `categories` value defined in the category page.

authors:
- Author Name

categories:
- Category Name

# Search Engine Optimisation
# ==========================
#
# `description`    Adds a <meta name="description" content="..."> tag to the
#                  HTML <head> section. Although debated, this is believed to
#                  improve SEO.

description: >
  Description of the page for SEO.

# Page Layout
# ===========
#
# `pagelayout`     Written defines two separate page layouts to separate content
#                  pages from other pages such as indexes. Use:
#                  "content-page"  for all posts,
#                  "index-page"    for all other pages.

pagelayout: content-page
---

Post content goes here (Markdown).