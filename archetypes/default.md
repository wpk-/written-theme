---
# Header
# ======
#
# `title`          Is the title as it will appear on the page.
# `subtitle`       Optionally prints some text under the title in a slightly
#                  smaller font.

title: The Default Page
subtitle: ""

# Navigation
# ==========
#
# `linktitle`      Is the text for navigation links to this page. If not set, it
#                  defaults to the `title`.
# `menu`           Site navigation. See also: http://gohugo.io/extras/menus/
#                  Written has two menus: "main" and "footer".
#                  Use `pre` to add a FontAwesome icon to the left of the link.

linktitle: Default
#menu:
#  main:
#    pre: "fa-check-square-o"
#    weight: -80
#  footer:
#    weight: -80

# Search Engine Optimisation
# ==========================
#
# `description`    Adds a <meta name="description" content="..."> tag to the
#                  HTML <head> section. Although debated, this is believed to
#                  improve SEO.

description: >
  Description of the page for SEO.

# Page Layout
# ===========
#
# `pagelayout`     Written defines two separate page layouts to separate content
#                  pages from other pages such as indexes. Use:
#                  "content-page"  for all posts,
#                  "index-page"    for all other pages.
# `standalone`     Suppress the block with links to related content that is
#                  displayed by default on content pages.
#                  `true`: no links are displayed
#                  `false` or omitted: links are displayed when pagelayout is
#                          content-page.

pagelayout: "content-page"
standalone: true
---
