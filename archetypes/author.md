---
# Header
# ======
#
# `title`          Is your name as it will appear on every page.
# `subtitle`       Optionally, in the header of the author page, prints some
#                  text under your name in a slightly smaller font.
# `feature_image`  Displays a tall image full height of the browser window to
#                  the left of the content. On smaller devices this moves to the
#                  top. Make sure this is a fairly big image to accommodate both
#                  views.

title: Author Name
subtitle: Writer of the past
feature_image: ""

# Semantics
# =========
#
# `authors`        Although a list, should inlcude only the author this page
#                  describes. The technical detail is that authors is a taxonomy
#                  and each individual author is modelled as a taxonomy term.
# `taxonomy`       Set this to the taxonomy this term belongs to: authors.
#
# NB. Your name here, as it appears in `authors`, is a *key*. It does not have
#     to be the same as `title`, but is certainly conventient. In a post use:
#
#     authors:
#     - Author Name
#     - Other Author Name
#     - ...

authors:
- Author Name
taxonomy: authors

# Social Links
# ============
#
# Under `links` you are free to use any key. Then, under that key:
# `title`          If `link` is used, is the text printed inside the link,
#                  otherwise just printed without link.
# `link`           URL to link to (optional).
# `rel`            In HTML you can use the rel tag to mark the relation of the
#                  linked document to the current page. Can be used to add
#                  "nofollow".
#
# Example:
#
#   links:
#     Twitter:
#       title: @Author_Name
#       link: //twitter.com/Author_Name
#       rel: nofollow
#
# produces
#
#   "Twitter: [@Author_Name](//twitter.com/Author_Name)"

links:
  Homepage:
    title:
    link:
  Twitter:
    title:
    link:

# Profile Photo
# =============
#
# `image`          Use this if you prefer an image avatar. Otherwise use a
#                  combination of the other three properties.
# `initials`       Use up to two characters. They will be printed in a circle.
# `bgcolor`        Background colour of the circle with initials.
# `textcolor`      Text colour of the initials inside the circle.

avatar:
  image: ""
  initials: "AN"
  bgcolor: "none"
  textcolor: "black"

# Search Engine Optimisation
# ==========================
#
# `description`    Adds a <meta name="description" content="..."> tag to the
#                  HTML <head> section. Although debated, this is believed to
#                  improve SEO.

description: >
  Description of the page for SEO.

# Page Layout
# ===========
#
# `pagelayout`     Written defines two separate page layouts to separate content
#                  pages from other pages such as indexes. Use:
#                  "content-page"  for all posts,
#                  "index-page"    for all other pages.

pagelayout: index-page
---

> A rose is a rose is a rose.
>
> -- <cite>Gertrude Stein</cite>

> **Juliet:**
> What's in a name? That which we call a rose, by any other name, would smell
> as sweet.
>
> -- <cite>Romeo and Juliet (II, ii, 1-2)</cite>

