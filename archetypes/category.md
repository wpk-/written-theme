---
# Header
# ======
#
# `title`          The category name as it will appear on every link and as the
#                  title of the category page.
# `subtitle`       Optionally, in the header of the category page, prints some
#                  text under the title in a slightly smaller font.
# `feature_image`  Displays a tall image full height of the browser window to
#                  the left of the content. On smaller devices this moves to the
#                  top. Make sure this is a fairly big image to accommodate both
#                  views.

title: Category Name
subtitle: ""
feature_image: ""

# Semantics
# =========
#
# `categories`     Although a list, should inlcude only the current category.
#                  The technical detail is that categories is a taxonomy and
#                  each category is modelled as a taxonomy term.
# `taxonomy`       Set this to the taxonomy this term belongs to: categories.
#
# NB. The category name as defined here in `categories`, is a *key*. It does not
#     have to be the same as `title`, but is certainly conventient. In a post
#     use:
#
#     categories:
#     - Category Name
#     - Other Category
#     - ...

categories:
- Category Name
taxonomy: categories


# Search Engine Optimisation
# ==========================
#
# `description`    Adds a <meta name="description" content="..."> tag to the
#                  HTML <head> section. Although debated, this is believed to
#                  improve SEO.

description: >
  Description of the page for SEO.

# Page Layout
# ===========
#
# `pagelayout`     Written defines two separate page layouts to separate content
#                  pages from other pages such as indexes. Use:
#                  "content-page"  for all posts,
#                  "index-page"    for all other pages.

pagelayout: index-page
---

A short text describing the **category**. Find a list of the *five most recent*
posts in this category below.
